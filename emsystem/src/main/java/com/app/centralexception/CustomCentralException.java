package com.app.centralexception;

public class CustomCentralException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2228599353267961224L;

	public CustomCentralException(String msg) {
		super(msg);
	}
}
