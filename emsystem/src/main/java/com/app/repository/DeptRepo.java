package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Department;

public interface DeptRepo extends JpaRepository<Department, Long> {
	Department save(Department dept);
}
