
package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.pojos.SkillSet;

@Repository
public interface SkillSetRepo extends JpaRepository<SkillSet, Long> {
	SkillSet save(SkillSet skillSet);
}
