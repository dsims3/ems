package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.pojos.JobProfile;

@Repository
public interface JobProfileRepo extends JpaRepository<JobProfile, Long> {
	JobProfile save(JobProfile jobProfile);
}
