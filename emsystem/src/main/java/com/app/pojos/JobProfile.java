package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="job_profiles")
public class JobProfile extends BaseEntity {
	@Column(name = "job_name",length = 50,nullable = false)
	private String jobName;
	
	@Column(name = "job_desc",nullable = false)
	private String jobDesc;

}
