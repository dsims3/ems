package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "skill_sets")
public class SkillSet extends BaseEntity {

	@Enumerated(EnumType.STRING)
	@Column(name = "skill_type", nullable = false)
	private Skill skill;

	@Column(name = "skill_name", length = 50, nullable = false)
	private String skillName;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private Proficiency proficiency;

	@Column(name = "exp", nullable = false)
	private int experience;

	@ManyToOne
	@JoinColumn(name = "emp_id")
	private Emp emp;

}