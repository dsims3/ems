package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@Getter
@Setter
@ToString
@Embeddable
public class Address {

	@NotNull(message = "Street Name cannot be blank")
	@Column(length = 100, nullable = false)
	private String street;

	@NotNull(message = "City Name cannot be blank")
	@Column(length = 100, nullable = false)
	private String city;

	@NotNull(message = "State Name cannot be blank")
	@Column(length = 100, nullable = false)
	private String state;

	@NotNull(message = "Pin Code cannot be blank")
	@Column(length = 15, nullable = false)
	private String pincode;

	@NotNull(message = "Please mention Nearby Landmark")
	@Column(length = 100, nullable = false)
	private String landmark;
}
