package com.app.pojos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
//@ToString(exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonInclude(Include.NON_NULL)
public class Emp extends User {

	@ManyToOne
	@JoinColumn(name = "dept_id", nullable = false)
	@JsonIgnoreProperties(value = { "emps" })
	private Department dept;

	@ManyToOne
	@JoinColumn(name = "job_profile_id", nullable = false)
	private JobProfile jobProfile;

	@OneToMany(mappedBy = "emp", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties(value = { "emp" })
	private List<SkillSet> skillsets = new ArrayList<>();

	@OneToMany(mappedBy = "emp", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties(value = { "emp" })
	private List<Salary> salaries = new ArrayList<Salary>();

	public void addSkillSet(SkillSet skillSet) {
		skillSet.setEmp(this);
	}

	public void removeSkillSet(SkillSet skillSet) {
		skillSet.setEmp(null);
	}

	public void addSalary(Salary salary) {
		salary.setEmp(this);
		this.salaries.add(salary);
	}

	public void removeSalary(Salary salary) {
		this.salaries.remove(salary);
		salary.setEmp(null);
	}

}
