package com.app.pojos;

public enum Proficiency {
	EXCELLENT, MODERATE, BEGINNER;
}
