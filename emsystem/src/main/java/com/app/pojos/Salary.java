package com.app.pojos;

import java.time.LocalDate;
import java.time.Month;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "salaries")
@JsonInclude(Include.NON_NULL)
public class Salary extends BaseEntity {

	@NotNull(message = "Basic Salary cannot be blank")
	@Column(nullable = false)
	private double basic;

	@NotNull(message = "House and Rent Allowance cannot be blank")
	@Column(nullable = false)
	private double hra;

	@Column
	private double bonus;

	@OneToOne
	@JoinColumn(name = "emp_id", nullable = false)
	private Emp emp;

	@Column(name = "salary_date", columnDefinition = "DATE", nullable = false)
	private LocalDate salaryDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "month", nullable = false)
	private Month month;
}
