package com.app.pojos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
//@ToString(exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "departments")
@JsonInclude(Include.NON_NULL)
public class Department extends BaseEntity {
	@Column(name = "title", length = 50, nullable = false, unique = true)
	private String deptTitle;

	@Column(name = "description")
	private String deptDesc;

	@OneToMany(mappedBy = "dept", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnoreProperties(value = { "dept", "skillsets", "salaries" })
	private List<Emp> emps = new ArrayList<>();

	public void addEmp(Emp employee) {
		this.emps.add(employee);
		employee.setDept(this);
	}

	public void removeEmp(Emp employee) {
		this.emps.remove(employee);
		employee.setDept(null);
	}
}
