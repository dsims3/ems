package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
//@ToString(exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
@JsonInclude(Include.NON_NULL)

public class User extends BaseEntity {

	@NotNull(message = "First Name cannot be blank")
	@Length(max = 20)
	@Column(name = "first_name", length = 20, nullable = false)
	protected String firstName;

	@NotNull(message = "Last Name cannot be blank")
	@Length(max = 25)
	@Column(name = "last_name", length = 25, nullable = false)
	protected String lastName;

	@NotNull(message = "Email cannot be blank")
	@Email(message = "Invalid email format!")
	@Column(length = 50, unique = true, nullable = false)
	protected String email;

	@NotNull(message = "Password cannot be null")
	// @Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[#@$*]).{5,20})", message =
	// "Invalid Password !")
	@JsonProperty(access = Access.WRITE_ONLY)
	@Column(length = 30, nullable = false)
	protected String password;

	@NotNull(message = "Phone Number must be provided")
	@Length(max = 10)
	@Column(length = 10, nullable = false, unique = true)
	protected String phoneNo;

	@NotNull(message = "Address cannot be null")
	@Embedded
	@JsonInclude
	protected Address address;

	@Enumerated(EnumType.STRING)
	@Column(name = "user_role")
	protected Role role;
}
