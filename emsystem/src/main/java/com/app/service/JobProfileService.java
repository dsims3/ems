package com.app.service;

import java.util.List;

import javax.validation.Valid;

import com.app.pojos.JobProfile;

public interface JobProfileService {

	JobProfile saveJobProfile(@Valid JobProfile jobProfile);

	JobProfile findJobProfile(Long id);

	List<JobProfile> loadAllJobProfiles();
}
