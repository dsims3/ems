package com.app.service;

import javax.validation.Valid;

import com.app.pojos.User;

public interface UserService {

	User saveUser(@Valid User user);

	User loadUser(String email);

}
