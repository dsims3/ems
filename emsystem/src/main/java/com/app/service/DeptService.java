package com.app.service;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.app.pojos.Department;

public interface DeptService {

	Department saveDept(@Valid Department dept);

	Department findDept(Long id);

	void saveAllDept(@Valid ArrayList<Department> depts);

	List<Department> loadAllDepartments();

}
