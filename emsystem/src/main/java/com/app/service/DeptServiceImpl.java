package com.app.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.centralexception.CustomCentralException;
import com.app.pojos.Department;
import com.app.repository.DeptRepo;

@Service
@Transactional
public class DeptServiceImpl implements DeptService {

	@Autowired
	DeptRepo deptRepo;

	@Override
	public Department saveDept(@Valid Department dept) {
		return deptRepo.save(dept);
	}

	@Override
	public Department findDept(Long id) {
		return deptRepo.findById(id).orElseThrow(() -> new CustomCentralException("Invalid Department ID!!!"));
	}

	@Override
	public void saveAllDept(@Valid ArrayList<Department> depts) {
		for (Department dept : depts)
			deptRepo.save(dept);

	}

	@Override
	public List<Department> loadAllDepartments() {
		return deptRepo.findAll();
	}

}
