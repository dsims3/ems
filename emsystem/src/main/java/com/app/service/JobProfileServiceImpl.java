package com.app.service;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.centralexception.CustomCentralException;
import com.app.pojos.JobProfile;
import com.app.repository.JobProfileRepo;

@Service
@Transactional
public class JobProfileServiceImpl implements JobProfileService {
	@Autowired
	JobProfileRepo jobProfileRepo;

	@Override
	public JobProfile saveJobProfile(@Valid JobProfile jobProfile) {
		return jobProfileRepo.save(jobProfile);
	}

	@Override
	public JobProfile findJobProfile(Long id) {
		return jobProfileRepo.findById(id).orElseThrow(() -> new CustomCentralException("Invalid JobProfile ID!!!"));
	}

	@Override
	public List<JobProfile> loadAllJobProfiles() {
		return jobProfileRepo.findAll();
	}
}