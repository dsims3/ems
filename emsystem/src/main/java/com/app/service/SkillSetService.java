package com.app.service;

import java.util.List;

import javax.validation.Valid;

import com.app.pojos.Emp;
import com.app.pojos.SkillSet;

public interface SkillSetService {

	void saveSkillSets(@Valid Emp emp,@Valid List<SkillSet> skillsets);

}
