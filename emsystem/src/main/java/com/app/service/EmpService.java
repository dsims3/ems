package com.app.service;

import javax.validation.Valid;

import com.app.pojos.Emp;

public interface EmpService {

	Emp saveEmp(@Valid Emp emp);

	Emp loadEmp(String email);

	String deleteEmp(String email);

	Emp updateEmp(Long id, Emp emp);

}
