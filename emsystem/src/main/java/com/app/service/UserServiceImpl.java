package com.app.service;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.pojos.User;
import com.app.repository.UserRepo;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepo userRepo;

	@Override
	public User saveUser(@Valid User transientUser) {
		User persistedUser = userRepo.save(transientUser);
		return persistedUser;
	}

	@Override
	public User loadUser(String email) {
		return userRepo.findByEmail(email);
	}
}