package com.app.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.pojos.Emp;
import com.app.pojos.SkillSet;
import com.app.repository.SkillSetRepo;

@Service
@Transactional
public class SkillSetServiceImpl implements SkillSetService {
	@Autowired
	SkillSetRepo skillSetRepo;

	@Override
	public void saveSkillSets(@Valid Emp emp, @Valid List<SkillSet> skillsets) {
		ArrayList<SkillSet> arr = new ArrayList<SkillSet>();
		for (SkillSet skillSet : skillsets) {
			SkillSet ss = skillSetRepo.save(skillSet);
			// Adding persistent SkillSet object to the 'still transient Emp object'
			// Setting up Emp for this SkillSet using helper method in Emp POJO
			emp.addSkillSet(ss);

			// Collecting for further addition
			arr.add(ss);
		}
		List<SkillSet> oldOne = emp.getSkillsets();
		oldOne.addAll(arr);
		emp.setSkillsets(oldOne);
	}

}