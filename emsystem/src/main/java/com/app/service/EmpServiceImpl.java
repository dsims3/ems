package com.app.service;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.centralexception.CustomCentralException;
import com.app.pojos.Department;
import com.app.pojos.Emp;
import com.app.pojos.JobProfile;
import com.app.repository.EmpRepo;

@Service
@Transactional
public class EmpServiceImpl implements EmpService {

	@Autowired
	EmpRepo empRepo;

	@Autowired
	SkillSetService skillSetService;

	@Autowired
	JobProfileService jobService;

	@Autowired
	DeptService deptService;

	@Override
	public Emp saveEmp(@Valid Emp emp) {
		skillSetService.saveSkillSets(emp, emp.getSkillsets());

		JobProfile jp = jobService.findJobProfile(emp.getJobProfile().getId());
		emp.setJobProfile(jp);
		Department dept = deptService.findDept(emp.getDept().getId());
		emp.setDept(dept);
		return empRepo.save(emp);
	}

	@Override
	public Emp loadEmp(String email) {
		return empRepo.findByEmail(email);
	}

	@Override
	public String deleteEmp(String email) {
		try {
			Emp emp = empRepo.findByEmail(email);
			Department dept = emp.getDept();
			// Removing Employee from Department
			dept.removeEmp(emp);
			// Saving updated Dept
			deptService.saveDept(dept);
			empRepo.delete(emp);
			return "Resorce Deleted Successfully!!!";
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomCentralException("Resource Deletion failed!!!");
		}

	}

	@Override
	public Emp updateEmp(Long id, Emp emp) {
		Emp originalEmp = empRepo.findById(id).orElseThrow(() -> new CustomCentralException("Invalid Employee ID!!!"));
		originalEmp.setAddress(emp.getAddress());
		originalEmp.setDept(emp.getDept());
		originalEmp.setEmail(emp.getEmail());
		originalEmp.setFirstName(emp.getFirstName());
		originalEmp.setJobProfile(emp.getJobProfile());
		originalEmp.setLastName(emp.getLastName());
		originalEmp.setPassword(emp.getPassword());
		originalEmp.setPhoneNo(emp.getPhoneNo());
		originalEmp.setRole(emp.getRole());
		originalEmp.setSalaries(emp.getSalaries());
		originalEmp.setSkillsets(emp.getSkillsets());
		empRepo.save(originalEmp);
		return originalEmp;
	}
}
