package com.app.controllers;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Department;
import com.app.service.DeptService;

@CrossOrigin
@RestController
@RequestMapping("/dept")
public class DeptController {
	@Autowired
	DeptService deptService;

	@PostMapping("/add")
	public ResponseEntity<?> addDept(@Valid @RequestBody Department dept) {
		return new ResponseEntity<>("Department ID :: " + deptService.saveDept(dept).getId(), HttpStatus.CREATED);
	}

	@PostMapping("/addall")
	public ResponseEntity<?> addAllDept(@Valid @RequestBody ArrayList<Department> depts) {
		deptService.saveAllDept(depts);
		return new ResponseEntity<>("Departments saved successfully!!!", HttpStatus.CREATED);
	}
	
	@GetMapping("/loadall")
	public ResponseEntity<?> loadAllDepartments() {
		return new ResponseEntity<>(deptService.loadAllDepartments(), HttpStatus.OK);
	}

}