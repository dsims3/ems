package com.app.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Credentails;
import com.app.pojos.User;
import com.app.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("/authenticate")
	public ResponseEntity<String> authenticate(@RequestBody Credentails cred) {

		return null;
	}

	@PostMapping("/register")
	public ResponseEntity<?> registerNewUser(@Valid @RequestBody User user) {
		return new ResponseEntity<>("User ID :: " + userService.saveUser(user).getId(), HttpStatus.CREATED);
	}
	
	@GetMapping
	public ResponseEntity<?> loadEmployee(@RequestBody String email) {
		return new ResponseEntity<>(userService.loadUser(email), HttpStatus.OK);
	}

}