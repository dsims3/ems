package com.app.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Credentails;
import com.app.pojos.Emp;
import com.app.service.EmpService;

@CrossOrigin
@RestController
@RequestMapping("/emp")
public class EmployeeController {

	@Autowired
	EmpService empService;

	@PostMapping("/authenticate")
	public ResponseEntity<String> authenticate(@RequestBody Credentails cred) {

		return null;
	}

	@PostMapping("/register")
	public ResponseEntity<?> registerNewEmployee(@Valid @RequestBody Emp emp) {
		return new ResponseEntity<>("Employee ID :: " + empService.saveEmp(emp).getId(), HttpStatus.CREATED);
	}

	@GetMapping
	public ResponseEntity<?> loadEmployee(@RequestBody String email) {
		return new ResponseEntity<>(empService.loadEmp(email), HttpStatus.OK);
	}

	@PutMapping("/id")
	public ResponseEntity<?> updateEmployee(@PathVariable Long id, @RequestBody Emp emp) {
		return new ResponseEntity<>(empService.updateEmp(id, emp), HttpStatus.OK);
	}

	@DeleteMapping
	public ResponseEntity<?> deleteEmployee(@RequestBody String email) {
		return new ResponseEntity<>(empService.deleteEmp(email), HttpStatus.OK);
	}

}