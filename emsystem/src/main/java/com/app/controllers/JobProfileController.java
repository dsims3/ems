package com.app.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.JobProfile;
import com.app.service.JobProfileService;

@CrossOrigin
@RestController
@RequestMapping("/job")
public class JobProfileController {
	@Autowired
	JobProfileService jobService;

	@PostMapping("/add")
	public ResponseEntity<?> addJobProfile(@Valid @RequestBody JobProfile jobProfile) {
		return new ResponseEntity<>("JobProfile ID :: " + jobService.saveJobProfile(jobProfile).getId(),
				HttpStatus.CREATED);
	}

	@GetMapping
	public ResponseEntity<?> loadAllJobProfiles() {
		return new ResponseEntity<>(jobService.loadAllJobProfiles(), HttpStatus.OK);
	}
}